package pl.sda.jav9ldz.servlets;

import pl.sda.jav9ldz.daos.ServerDAO;
import pl.sda.jav9ldz.daos.UserDAO;
import pl.sda.jav9ldz.models.Server;
import pl.sda.jav9ldz.utils.CommonUtils;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "ServerListServlet", urlPatterns = "/showServerList")
public class ServerListServlet extends HttpServlet {


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        List<Server> allServers = ServerDAO.getAllServers();
        request.setAttribute("allServers", allServers);

        request.setAttribute("allUsers", CommonUtils.createUserMap(UserDAO.getAllUsers()));

        RequestDispatcher requestDispatcher = request.getRequestDispatcher("/serverList.jsp");
        requestDispatcher.forward(request, response);
    }
}
