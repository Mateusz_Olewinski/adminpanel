<%@ page import="pl.sda.jav9ldz.models.User" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Wylogowano</title>
</head>
<body>
<%
User user = (User) request.getAttribute("user");
%>
<h1>Użytkownik <%=user.getName()%> został poprawnie wylogowany</h1>
<br>
<a href="/login.jsp"> Zaloguj</a>
</body>
</html>
